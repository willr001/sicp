(define (inc x) (+ x 1))
(define (dec x) (- x 1))
(define (square x) (* x x))
(define (double x) (* x 2 ))
(define (halve x) (/ x 2))

