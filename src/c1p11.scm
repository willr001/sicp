(define (f n)
  (cond
    ((< n 3) n)
    (else
      (+ (* 1 (f (- n 1)))
         (* 2 (f (- n 2)))
         (* 3 (f (- n 3)))))))
(define (f-calc p pp ppp)
  (+ p
     (* 2 pp)
     (* 3 ppp)))
(define (f-iter n count p pp ppp)
  (cond
    ((< count n) (f-iter n (+ 1 count) (f-calc p pp ppp) p pp))
    (else (f-calc p pp ppp))))
(define (f n) (f-iter n 0 0 0 0))

