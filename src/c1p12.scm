(define (pascals-triangle row col) 
  (cond
    ((< row 0) 0)
    ((< col 0) 0)
    ((> col row) 0)
    ((and (= row 0) (= col 0)) 1)
    (else (+ (pascals-triangle (- row 1) col) (pascals-triangle (- row 1) (- col 1))))))
